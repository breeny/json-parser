FROM node:10.16-alpine
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN yarn
CMD ["yarn", "start"]