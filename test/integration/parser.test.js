const axios = require("axios");
const fs = require("fs");

const APP_URL = process.env.APP_URL || "http://localhost:3000";

describe("JSON parser", () => {
  it("handles the provided JSON as expected", async () => {
    const request = require("./data/sample-request.json");
    const expectedResponse = require("./data/sample-response.json");

    const response = await axios.post(APP_URL, request);
    const responseBody = response.data;
    expect(responseBody).toEqual(expectedResponse);
  });

  it('handles if image object is empty with a placeholder string', async () => {
    const request = require("./data/sample-request-2.json");
    const expectedResponse = require("./data/sample-response-2.json");

    const response = await axios.post(APP_URL, request);
    const responseBody = response.data;
    expect(responseBody).toEqual(expectedResponse);
  });

  test.each([["invalid_1.json"], ["invalid_2.json"]])(
    "it handles invalid request: %s",
    fileName => {
      const req = fs.readFileSync(`./data/${fileName}`);
      expect.assertions(2);
      return axios.post(APP_URL, req).catch(err => {
        expect(err.response.status).toEqual(400);
        expect(err.response.data).toEqual({
          error: "Could not decode request"
        });
      });
    }
  );
});
