/**
 * app.js
 */

const express = require("express");
const bodyparser = require("body-parser");

const app = express();

app.use(bodyparser.json());

app.post("/", (req, res) => {
  const payload = req.body.payload;
  const result = payload
    .filter(show => show && show.drm && show.episodeCount > 0)
    .map(show => ({
      image: show.image ? show.image.showImage : "http://mybeautifulcatchupservice.com/img/placeholder",
      slug: show.slug,
      title: show.title
    }));
  res.status(200).send({
    response: result
  });
});

//Error handling middleware
app.use((err, req, res, next) => {
  // This should be handled better by a proper logging service/provider
  // It can then be abstracted in integration tests too to prevent extra noise
  console.error(err);
  res.status(400).send({
    error: "Could not decode request"
  });
});

//Start the server on env port or 3000 locally
app.listen(process.env.PORT || 3000, () => {
  console.log(`Server started on port 3000`);
});
